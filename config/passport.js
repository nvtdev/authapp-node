const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const config = require('../config/database');

module.exports = function(passport){
  let opts = {};
  opts.jwtFromRequest = ExtractJWT.fromAuthHeader();
  opts.secretOrKey = config.secret;
  passport.use(new JWTStrategy(opts, (jwt_payload, done) => {
    // beware of the ._doc below, if it doesn't work uncomment
    // the line below and check where the id is stored exactly:
    // console.log(jwt_payload);
    User.getUserById(jwt_payload._doc._id, (err, user) => {
      if (err) return done(err, false);
      if (user) return done(null, user)
           else return done(null, false);
    })
  }))
}
